package me.vinceh121.shutupbot;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.FormattedMessage;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.entities.emoji.CustomEmoji;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.MessageUpdateEvent;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

public class ShutUpBot extends ListenerAdapter {
	private static final Logger LOG = LogManager.getLogger(ShutUpBot.class);
	private static final ObjectMapper MAPPER = new ObjectMapper();
	private static final Pattern WORD_PATTERN = Pattern.compile("[a-zA-Z0-9"
			+ "\\u00C0-\\u00D6" // uppercase latin 1 supplement letters
			+ "\\u00D8-\\u00F6" // lowercase latin 1 supplement letters
			+ "\\u00F8-\\u00FF" // lowercase latin 1 supplement letters 2nd part
			+ "\\p{InCyrillic}\\p{InGreek}\\p{InHiragana}\\p{InKatakana}"
			+ "\\p{InHangulSyllables}\\p{InCjkUnifiedIdeographs}]+");
	private static final Pattern[] INSTAGRAM_PATTERNS
			= { Pattern.compile(Pattern.quote("https://www.instagram.com/reel/") + "([a-zA-Z0-9\\-_]+)"),
					Pattern.compile(Pattern.quote("https://www.instagram.com/p/") + "([a-zA-Z0-9\\-_]+)") };
	private static final Pattern[] TIKTOK_PATTERNS
			= { Pattern.compile(Pattern.quote("https://vm.tiktok.com/") + "([a-zA-Z0-9\\-_]+)"),
					Pattern.compile(Pattern.quote("https://www.tiktok.com/@[a-zA-Z0-9\\-_]/video/") + "(0-9)+") };
	private static final Pattern[] TWITTER_STATUS_PATTERNS = { Pattern.compile(
			Pattern.quote("https://twitter.com/") + "([a-zA-Z0-9\\-_]+)" + Pattern.quote("/status/") + "([0-9]+)"),
			Pattern.compile(Pattern.quote("https://fxtwitter.com/")
					+ "([a-zA-Z0-9\\-_]+)"
					+ Pattern.quote("/status/")
					+ "([0-9]+)") };
	private static final Pattern[] TWITTER_ACCOUNT_PATTERNS
			= { Pattern.compile(Pattern.quote("https://twitter.com/") + "([a-zA-Z0-9\\-_]+)") };
	private final ShutUpConfig config;
	private final JDA jda;
	private final CustomEmoji squintEmote;
	private final List<Member> mutedMembersCache = new Vector<>();
	private final LevenshteinDistance distance = new LevenshteinDistance();

	private final Pattern dadPattern;

	public static void main(final String[] args) {
		try {
			new ShutUpBot();
		} catch (final Exception e) {
			LOG.error("Failed to start bot", e);
			System.exit(-69);
		}
	}

	public ShutUpBot() throws Exception {
		this.config = MAPPER.readValue(new File("/etc/shutup.json"), ShutUpConfig.class);
		this.dadPattern = Pattern.compile(
				"(" + String.join("|", config.getDadWords()) + ")\\p{Space}+([^\\p{Punct}]+)(\\p{Punct}*)",
				Pattern.CASE_INSENSITIVE);
		this.jda = JDABuilder.createLight(this.config.getToken())
				.setEnableShutdownHook(false)
				.enableIntents(GatewayIntent.MESSAGE_CONTENT)
				.enableCache(CacheFlag.EMOJI) // i hate having to cache emotes just for the stupid bot reaction
				.build();
		this.jda.awaitReady();
		LOG.info("Reached ready!");
		this.squintEmote = this.jda.getEmojiById(this.config.getSquintEmote());
		this.jda.addEventListener(this);
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			if (this.mutedMembersCache.size() != 0) {
				LOG.warn("Shutting down with members still muted, trying to unmute: {}", this.mutedMembersCache);
				for (final Member m : this.mutedMembersCache) {
					m.getGuild().removeTimeout(m).complete();
				}
			}
			this.jda.shutdown();
		}, "ShutUpShutdown"));
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		if (event.isFromGuild()) {
			this.onGuildMessageReceived(event);
		} else if (event.getChannel().getType() == ChannelType.PRIVATE) {
			this.onPrivateMessageReceived(event);
		}
	}

	public void onPrivateMessageReceived(final MessageReceivedEvent event) {
		if (event.getAuthor().isBot()) {
			return;
		}

		final boolean isMuted = this.isMuted(event.getAuthor().getIdLong());

		if (!isMuted) {
			event.getChannel().sendMessage("Youw'we not even puwnished? :v").queue();
			return;
		} else if (isMuted && this.distance.apply(event.getMessage().getContentStripped(), "sorry") > 2) {
			event.getChannel().sendMessage("Shuwsh! Youw'we puwnished ÒwÓ\n\nApowogize.").queue();
			return;
		}

		for (final Member m : new ArrayList<>(this.mutedMembersCache)) {
			if (m.getUser().getIdLong() == event.getAuthor().getIdLong()) {
				LOG.info("Unmutting because of apology {}", m);
				m.getGuild().removeTimeout(m).complete();
				this.mutedMembersCache.remove(m);
			}
		}

		event.getChannel().sendMessage("Youw'we pawdoned.\n\n" + "dyon't dewit agwain >:(").queue();
	}

	private boolean isMuted(final long id) {
		for (final Member m : this.mutedMembersCache) {
			if (m.getUser().getIdLong() == id) {
				return true;
			}
		}
		return false;
	}

	public void onGuildMessageUpdate(final MessageUpdateEvent event) {
		this.onGuildMessageReceived(new MessageReceivedEvent(this.jda, -1, event.getMessage()));
	}

	public void onGuildMessageReceived(final MessageReceivedEvent event) {
		this.doDad(event);
		this.doBibliogram(event);
		this.doProxytok(event);
		this.doNitter(event);
		this.doIllegalWords(event);
	}

	private void doBibliogram(final MessageReceivedEvent event) {
		if (!this.config.isEnableBibliogram()) {
			return;
		}

		if (event.getAuthor().isBot()) {
			return;
		}

		for (Pattern p : ShutUpBot.INSTAGRAM_PATTERNS) {
			final Matcher matcher = p.matcher(event.getMessage().getContentRaw());
			final List<String> links = new Vector<>();

			while (matcher.find()) {
				LOG.info("Sending instagram message for {}", matcher.group(1));
				links.add("https://bibliogram.art/p/" + matcher.group(1));
			}

			if (links.size() == 0) {
				continue;
			}

			event.getChannel()
					.sendMessage("OwO wooks like u sent swomwe fwucky-wucky instwagwam links, "
							+ "here's a better way two open them on desktwop:\n\n"
							+ String.join("\n", links))
					.queue();
		}
	}

	private void doProxytok(final MessageReceivedEvent event) {
		if (!this.config.isEnableProxitok()) {
			return;
		}

		if (event.getAuthor().isBot()) {
			return;
		}

		for (Pattern p : ShutUpBot.TIKTOK_PATTERNS) {
			final Matcher matcher = p.matcher(event.getMessage().getContentRaw());
			final List<String> links = new Vector<>();

			while (matcher.find()) {
				LOG.info("Sending tiktok message for {}", matcher.group(1));
				links.add("https://" + this.config.getProxitokInstance() + "/@placeholder/video/" + matcher.group(1));
			}

			if (links.size() == 0) {
				continue;
			}

			event.getChannel()
					.sendMessage("sighhhh u didn't learn ywour lesswon with instagwam did u?\n"
							+ "here's a better tiktwok link fwor desktwop:\n\n"
							+ String.join("\n", links))
					.queue();
		}
	}

	private void doNitter(MessageReceivedEvent event) {
		if (!this.config.isEnableNitter()) {
			return;
		}

		if (event.getAuthor().isBot()) {
			return;
		}

		List<String> links = new Vector<>();

		for (Pattern p : ShutUpBot.TWITTER_STATUS_PATTERNS) {
			Matcher matcher = p.matcher(event.getMessage().getContentRaw());

			while (matcher.find()) {
				LOG.info("Sending nitter status message for {}", matcher.group(1));
				links.add("https://"
						+ this.config.getNitterInstance()
						+ "/"
						+ matcher.group(1)
						+ "/status/"
						+ matcher.group(2));
			}
		}

		// don't process account links if we already have a status to avoid incorrect
		// matching
		if (links.size() == 0) {
			for (Pattern p : ShutUpBot.TWITTER_ACCOUNT_PATTERNS) {
				Matcher matcher = p.matcher(event.getMessage().getContentRaw());

				while (matcher.find()) {
					LOG.info("Sending nitter account message for {}", matcher.group(1));
					links.add("https://" + this.config.getNitterInstance() + "/" + matcher.group(1));
				}
			}
		}

		if (links.size() == 0) {
			return;
		}

		if (this.config.isButtonNitter()) {
			final List<Button> btns = new ArrayList<>(links.size());

			for (final String link : links) {
				btns.add(Button.link(link, "Open in Nitter"));
			}

			event.getMessage()
					.replyComponents(ActionRow.of(btns))
					.mentionRepliedUser(false)
					.setSuppressedNotifications(true)
					.queue();
		} else {
			if (this.config.isNoEmbedNitter()) {
				for (int i = 0; i < links.size(); i++) {
					links.set(i, "<" + links.get(i) + ">");
				}
			}

			event.getChannel()
					.sendMessage("OwO wooks like u sent swomwe fwucky-wucky twittew links, "
							+ "here's a better way two open them withouwt aww the buwwwshit:\n"
							+ String.join("\n", links))
					.queue();
		}
	}

	private void doDad(final MessageReceivedEvent event) {
		if (!this.config.isEnableDad()) {
			return;
		}

		if (event.getAuthor().isBot()) {
			return;
		}

		final Matcher matcher = this.dadPattern.matcher(event.getMessage().getContentStripped());
		final List<String> nicks = new Vector<>();

		while (matcher.find()) {
			final String
			// trigger = matcher.group(1),
			text = matcher.group(2), nickname;

			if (text.length() > 32) {
				nickname = text.substring(0, 32);
			} else {
				nickname = text;
			}

			nicks.add(nickname);
		}

		if (nicks.size() == 0) {
			return;
		}

		if (RandomUtils.nextFloat(0, 1) < this.config.getDadChance()) {
			return;
		}

		final String nickname = nicks.get(RandomUtils.nextInt(0, nicks.size())).trim();
		LOG.info("Changing nickname of {} to `{}`", event.getMember(), nickname);
		event.getChannel()
				.sendMessage("Hewwo "
						+ nickname
						+ "!\n\n"
						+ this.config.getDadJokes().get(RandomUtils.nextInt(0, this.config.getDadJokes().size())))
				.queue(m -> {
					if (RandomUtils.nextFloat(0, 1) < this.config.getDadRenameChance()) {
						return;
					}
					try {
						event.getMember().modifyNickname(nickname).queue(null, t -> {
							LOG.error(new FormattedMessage("Failed to edit nickname of {} for message {}",
									event.getMember(), event.getMessage()));
							event.getChannel()
									.sendMessage(
											"Owww nywoooo! Wooks like I wasn't able two change ywour nyicknyamwe ;-;\n"
													+ "can u pwease awwow mwe two change ywour nyicknyamwe..? <:________________:831723381069185044>")
									.setMessageReference(m)
									.queue();
						});
					} catch (final HierarchyException e) {
						LOG.error("Editing nickname not allowed", e);
						event.getChannel()
								.sendMessage(
										"Oh nywo, wooks like I'm nywot awwowed two change ywour nyicknyamwe... <:________________:831723381069185044>\n"
												+ "Hewwo nywot awwowed two change ywour nyicknyamwe!")
								.setMessageReference(m)
								.queue();
					} catch (final InsufficientPermissionException e) {
						LOG.error("Editing nickname not allowed", e);
						event.getChannel()
								.sendMessage(
										"pwease can swomweonye give mwe permission two change nyicknyamwes..? <:________________:831723381069185044>")
								.setMessageReference(m)
								.queue();
					}
				}, t -> LOG.error("Failed to send dad message", t));
	}

	private void doIllegalWords(final MessageReceivedEvent event) {
		final List<String> usedWords
				= this.config.isTypoTolerence() ? this.countTypoTolerence(event) : this.countStrict(event);

		if (usedWords.size() == 0) {
			return;
		}

		if (event.getAuthor().getIdLong() == this.jda.getSelfUser().getIdLong()) {
			return;
		}

		if (event.getAuthor().isBot()) {
			if (event.getAuthor().getIdLong() != this.jda.getSelfUser().getIdLong()) {
				event.getMessage().addReaction(this.squintEmote).queue();
			}
			return;
		}

		final long muteTime = this.config.getMuteTime() * usedWords.size();

		LOG.info("Muting a dirty mouther: {} for {} s for words {}", event.getMember(), muteTime, usedWords);

		final String msgContent = "Hey "
				+ event.getAuthor().getAsMention()
				+ ", wooks wike you said some wowds ffat awen't weawwy good. You'we getting muted fow "
				+ TimeUnit.MINUTES.convert(muteTime, TimeUnit.SECONDS)
				+ " minyutes.\n\n"
				+ "The bad wowds you said awe: "
				+ String.join(", ", usedWords);

		event.getChannel()
				.sendMessage(msgContent.length() > 2000 ? msgContent.substring(0, 1997) + "..." : msgContent)
				.queue();

		try {
			event.getGuild().timeoutFor(event.getMember(), muteTime, TimeUnit.SECONDS).queue(null, t -> {
				LOG.error(new FormattedMessage("Failed to mute {}", event.getMember()), t);
				event.getChannel().sendMessage("oh nyo i couwdn't set youw punyishment >~<").queue();
			});
		} catch (final HierarchyException e) {
			LOG.error("Cannot mute higher being {}", event.getMember());
			event.getChannel().sendMessage("sowwy mastew >w<").queue();
		} catch (final IllegalArgumentException e) {
			LOG.error("Bot operating in wrong guild", e);
			event.getChannel().sendMessage("ffis doesn't seem to be ffe guiwd i shouwd be in").queue();
		} catch (final InsufficientPermissionException e) {
			LOG.error("Missing permissions", e);
			event.getChannel().sendMessage("oh nyo i'm nyot awwowed to mute you ;-;").queue();
		}

		if (usedWords.size() == 1 && usedWords.get(0).equalsIgnoreCase(event.getMessage().getContentRaw())) {
			try {
				event.getMessage().delete().queue();
			} catch (final InsufficientPermissionException | IllegalStateException e) {
				LOG.error(new FormattedMessage("Couldn't delete message {}", event.getMessage().getIdLong()), e);
				event.getMessage().addReaction(this.squintEmote).queue();
			}
		}
	}

	private List<String> countStrict(final MessageReceivedEvent event) {
		final List<String> usedWords = new Vector<>();
		final Matcher matcher = WORD_PATTERN.matcher(event.getMessage().getContentRaw().toLowerCase());

		while (matcher.find()) {
			final String w = matcher.group();

			if (this.config.getIllegalWords().contains(w)) {
				usedWords.add(w);
			}
		}

		return usedWords;
	}

	private List<String> countTypoTolerence(final MessageReceivedEvent event) {
		final List<String> usedWords = new Vector<>();
		final Matcher matcher = WORD_PATTERN.matcher(event.getMessage().getContentRaw().toLowerCase());

		while (matcher.find()) {
			final String w = matcher.group();
			final Map<String, Integer> dists = new HashMap<>();

			for (final String iw : this.config.getIllegalWords()) {
				dists.put(iw, this.distance.apply(w, iw));
			}

			final String mlWord
					= Collections.min(dists.keySet(), (v1, v2) -> Integer.compare(dists.get(v1), dists.get(v2)));
			final int mlDist = dists.get(mlWord);

			if (mlDist <= 1) {
				usedWords.add(w + (mlDist != 0 ? " (guessing '" + mlWord + "')" : ""));
			}
		}

		return usedWords;
	}
}
