package me.vinceh121.shutupbot;

import java.util.Collections;
import java.util.List;

public class ShutUpConfig {
	private String token, nitterInstance, proxitokInstance;
	private List<String> illegalWords = Collections.emptyList(), dadWords = Collections.emptyList(),
			dadJokes = Collections.emptyList();
	private long squintEmote;
	private boolean typoTolerence, enableDad, enableBibliogram, enableProxitok, enableNitter, noEmbedNitter, buttonNitter;
	private float dadChance, dadRenameChance;

	/**
	 * seconds
	 */
	private long muteTime;

	public String getToken() {
		return token;
	}

	public void setToken(final String token) {
		this.token = token;
	}

	public String getNitterInstance() {
		return nitterInstance;
	}

	public void setNitterInstance(String nitterInstance) {
		this.nitterInstance = nitterInstance;
	}

	public List<String> getIllegalWords() {
		return illegalWords;
	}

	public void setIllegalWords(final List<String> illegalWords) {
		this.illegalWords = illegalWords;
	}

	public List<String> getDadWords() {
		return dadWords;
	}

	public void setDadWords(List<String> dadWords) {
		this.dadWords = dadWords;
	}

	public List<String> getDadJokes() {
		return dadJokes;
	}

	public void setDadJokes(List<String> dadJokes) {
		this.dadJokes = dadJokes;
	}

	/**
	 * seconds
	 * 
	 * @return
	 */
	public long getMuteTime() {
		return muteTime;
	}

	public void setMuteTime(long muteTime) {
		this.muteTime = muteTime;
	}

	public long getSquintEmote() {
		return squintEmote;
	}

	public void setSquintEmote(long squintEmote) {
		this.squintEmote = squintEmote;
	}

	public boolean isTypoTolerence() {
		return typoTolerence;
	}

	public void setTypoTolerence(boolean typoTolerence) {
		this.typoTolerence = typoTolerence;
	}

	public boolean isEnableDad() {
		return enableDad;
	}

	public void setEnableDad(boolean enableDad) {
		this.enableDad = enableDad;
	}

	public boolean isEnableBibliogram() {
		return enableBibliogram;
	}

	public void setEnableBibliogram(boolean enableBibliogram) {
		this.enableBibliogram = enableBibliogram;
	}

	public boolean isEnableProxitok() {
		return enableProxitok;
	}

	public void setEnableProxitok(boolean enableProxitok) {
		this.enableProxitok = enableProxitok;
	}

	public boolean isEnableNitter() {
		return enableNitter;
	}

	public void setEnableNitter(boolean enableNitter) {
		this.enableNitter = enableNitter;
	}

	public boolean isNoEmbedNitter() {
		return noEmbedNitter;
	}

	public void setNoEmbedNitter(boolean noEmbedNitter) {
		this.noEmbedNitter = noEmbedNitter;
	}

	public boolean isButtonNitter() {
		return buttonNitter;
	}

	public void setButtonNitter(boolean buttonNitter) {
		this.buttonNitter = buttonNitter;
	}

	public float getDadChance() {
		return dadChance;
	}

	public void setDadChance(float dadChance) {
		this.dadChance = dadChance;
	}

	public float getDadRenameChance() {
		return dadRenameChance;
	}

	public void setDadRenameChance(float dadRenameChance) {
		this.dadRenameChance = dadRenameChance;
	}

	public String getProxitokInstance() {
		return proxitokInstance;
	}

	public void setProxitokInstance(String proxitokInstance) {
		this.proxitokInstance = proxitokInstance;
	}
}
