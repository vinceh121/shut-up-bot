package me.vinceh121.shutupbot.cli;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogToCsv {
	private static final Pattern LOG_ENTRY_PATTERN = Pattern.compile(
			"Muting a dirty mouther: MB:.*\\(U:(.*)\\(([0-9]+)\\) / G:.*\\) for ([0-9]*) s for words \\[(.*)\\]");

	public static void main(final String[] args) throws IOException {
		if (args.length == 0) {
			System.err.println("Reading from stdin");
		} else {
			System.err.println("Reading from \"" + args[0] + "\"");
		}
		final BufferedReader bf = args.length == 0 ? new BufferedReader(new InputStreamReader(System.in))
				: new BufferedReader(new FileReader(args[0]));

		// number of times each ID appears
		final Map<String, AtomicLong> occurances = new Hashtable<>(), totalTime = new Hashtable<>();
		// ID by username
		final Map<String, String> usernames = new Hashtable<>();

		String line;
		while ((line = bf.readLine()) != null) {
			final Matcher match = LOG_ENTRY_PATTERN.matcher(line);
			if (!match.find()) {
				continue;
			}
			for (int i = 1; i <= match.groupCount(); i++) {
				// reformat word list to not use commas
				System.out.print(
						i == match.groupCount() ? match.group(i).replaceAll(Pattern.quote(", "), ";") : match.group(i));
				if (i != match.groupCount()) {
					System.out.print(",");
				}

			}
			if (!occurances.containsKey(match.group(2))) {
				occurances.put(match.group(2), new AtomicLong());
			}
			occurances.get(match.group(2)).incrementAndGet();
			
			if (!totalTime.containsKey(match.group(2))) {
				totalTime.put(match.group(2), new AtomicLong());
			}
			totalTime.get(match.group(2)).addAndGet(Integer.parseInt(match.group(3)));

			usernames.put(match.group(2), match.group(1));
			System.out.println();
		}
		
		System.out.println("\n\nUsername,Total mutes");
		for (final Entry<String, AtomicLong> e : occurances.entrySet()) {
			System.out.println(usernames.get(e.getKey()) + "," + e.getValue());
		}
		
		System.out.println("\n\nUsername,Total time (s)");
		for (final Entry<String, AtomicLong> e : totalTime.entrySet()) {
			System.out.println(usernames.get(e.getKey()) + "," + e.getValue());
		}
	}
}
