import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.text.similarity.EditDistance;
import org.apache.commons.text.similarity.LevenshteinDistance;

public class TextComparisonTest {
	private static final String[][] WORD_TABLE
			= { { "chico", "chica" }, { "chico", "chico" }, { "chico", "chier" }, { "chicas", "chicis" } };
	private static final String[] ILLEGAL_WORDS = { "chica", "chico", "chicas", "chicos" };

	public static void main(String[] args) throws IOException {
		final EditDistance<Integer> dis = new LevenshteinDistance();
		System.out.println(dis.apply("ch!ca", "chica"));
		Files.lines(Paths.get("/home/vincent/fr.dict")).forEach(s -> {
			for (final String il : ILLEGAL_WORDS) {
				if (s.length() == il.length()) {
					final int d = dis.apply(s, il);
					if (d <= 1)
						System.out.println(s + " - " + il + " -> " + d);
				}
			}
		});
	}
}
